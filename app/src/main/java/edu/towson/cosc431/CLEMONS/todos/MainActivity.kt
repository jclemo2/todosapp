package edu.towson.cosc431.CLEMONS.todos

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import com.google.gson.Gson
import edu.towson.cosc431.CLEMONS.todos.R.id.addTodoButton
import edu.towson.cosc431.CLEMONS.todos.R.id.recyclerView2
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_new_todo.*
import kotlinx.android.synthetic.main.activity_save_todos.*

class MainActivity : AppCompatActivity(), IController {

    companion object {
        val ADD_TODO_REQUEST_CODE = 1
    }

    override fun launchAddTodoScreen() {
        val intent = Intent(this, NewTodoActivity::class.java)
        startActivityForResult(intent, ADD_TODO_REQUEST_CODE)
    }

    override fun addTodo(todo: SaveTodos){
        todoList.add(todo)
    }

    var todoList: MutableList<SaveTodos> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        addTodoButton.setOnClickListener { launchAddTodoScreen() }

        val todoAdapter = Adapter(todoList)

        recyclerView2.layoutManager = LinearLayoutManager(this)

        recyclerView2.adapter = Adapter(todoList)

        populateTodoList()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode) {
            ADD_TODO_REQUEST_CODE -> {
                when(resultCode) {
                    Activity.RESULT_OK -> {
                        val json = data?.extras?.get(NewTodoActivity.TODO_EXTRA) as String?
                        if(json != null) {
                            val todo = Gson().fromJson<SaveTodos>(json, SaveTodos::class.java)
                            todoList.add(todo)
                        }
                    }
                    Activity.RESULT_CANCELED -> {
                        Toast.makeText(this, "Add Todo cancelled", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }

    private fun populateTodoList() {
        (1..10).forEach {
            todoList.add(SaveTodos("Title"+it, "Text"+it, it%3==0))
        }
    }
}
