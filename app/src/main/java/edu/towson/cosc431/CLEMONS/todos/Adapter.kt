package edu.towson.cosc431.CLEMONS.todos

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.activity_save_todos.view.*

class Adapter(val todoList: List<SaveTodos>) : RecyclerView.Adapter<TodoViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodoViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.activity_save_todos, parent, false)
        return TodoViewHolder(view)
    }

    override fun getItemCount(): Int {
        return todoList.size
    }

    override fun onBindViewHolder(holder: TodoViewHolder, position: Int) {
        val todo = todoList.get(position)
        holder.itemView.titleName.text = todo.title
        holder.itemView.textName.text = todo.textt
        holder.itemView.isCompleted.isChecked = todo.completed
    }
}

class TodoViewHolder(view: View) : RecyclerView.ViewHolder(view)