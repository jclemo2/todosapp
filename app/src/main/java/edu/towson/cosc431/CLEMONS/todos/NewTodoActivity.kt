package edu.towson.cosc431.CLEMONS.todos

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_new_todo.*


class NewTodoActivity : AppCompatActivity() {

    companion object {
        val TODO_EXTRA = "todo_extra"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_todo)
        addTodoBtn.setOnClickListener { sendTodoBack() }
    }

    private fun sendTodoBack() {

        val todo = SaveTodos(
                inputTitle.editableText.toString(),
                inputText.editableText.toString(),
                isCompleted.isChecked
        )
        if (!todo.isValid()) {
            Toast.makeText(this, todo.getError(), Toast.LENGTH_SHORT).show()
        } else {

            val bundle = Intent()
            bundle.putExtra(TODO_EXTRA, todo.toJson())
            setResult(Activity.RESULT_OK, bundle)
            finish()
        }
    }
}