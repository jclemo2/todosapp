package edu.towson.cosc431.CLEMONS.todos

import com.google.gson.Gson

data class SaveTodos(val title: String,
                     val textt: String,
                     val completed: Boolean) {

    fun toJson(): String {
        return Gson().toJson(this)
    }

    fun isValid(): Boolean {
        return title.isNotEmpty() && textt.isNotEmpty()
    }

    fun getError(): String {
        if(title.isEmpty()) {
            return "Todo name is empty"
        }
        if(textt.isEmpty()) {
            return "Todo text is empty"
        }
        throw Exception("Todo was valid")
    }
}




