package edu.towson.cosc431.CLEMONS.todos

interface IController {
    fun launchAddTodoScreen()
    fun addTodo(todo: SaveTodos)

}